Integração com a API da eRede em C#

Esta solução contém um projeto que gera uma DLL para comunicação com a API da eRede, e também um projeto de Demonstração.

Manual 
https://www.userede.com.br/desenvolvedores/Paginas/manual.html

Para ambiente de homologação, deve-se utilizar as seguintes configurações:

- Filiação: 50079557
- Token: 4913bb24a0284954be72c4258e229b86

Além disso para testar as transações, a eRede disponibiliza um arquivo excel com algumas configurações para as transações. Abaixo segue link do arquivo.

https://www.userede.com.br/pt-BR/Lists/Downloads/Attachments/91/CardsTESTE_Redecard.zip
Senha: beta2016
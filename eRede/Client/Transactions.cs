﻿using eRede.Api;
using eRede.Model;
using RestSharp;
using System;
using System.Collections.Generic;

namespace eRede.Client
{
    public class Transactions
    {
        private ApiClient ApiClient = null;
        private string AccessToken = "";

        public Transactions(string affiliation, string token, EnvironmentType environmentType)
        {
            this.ApiClient = new ApiClient(environmentType);
            
            this.AccessToken = "Basic " + this.ApiClient.Base64Encode($"{affiliation}:{token}");
        }
        
        /// <summary>
        /// Captura uma Transação 
        /// </summary>
        /// <param name="tid">IdTransação</param> 
        /// /// <param name="amount">Valor sem separador de milhar e decimal</param>
        /// <returns>TransactionResponse</returns>
        public TransactionResponse CaptureTransaction(string tid, int amount)
        {
            #region Validations
            // verify the required parameter 'tid' is set
            if (string.IsNullOrEmpty(tid))
                throw new ApiException(400, "Missing required parameter 'tid' when calling CaptureTransaction");

            // verify the required parameter 'amount' is set
            if (amount <= 0)
                throw new ApiException(400, "Missing required parameter 'amount' when calling CaptureTransaction");
            #endregion

            var path = $"/transactions/{tid}";

            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();

            String postBody = "{ \"amount\": " + amount.ToString() + " }"; // http body (model) parameter

            headerParams.Add("authorization", ApiClient.ParameterToString(this.AccessToken)); // header parameter

            // authentication setting, if any
            String[] authSettings = new String[] { };

            // make the HTTP request
            IRestResponse response = (IRestResponse)ApiClient.CallApi(path, Method.PUT, queryParams, postBody, headerParams, formParams, fileParams, authSettings);

            TransactionResponse transResp = (TransactionResponse)ApiClient.Deserialize(response.Content, typeof(TransactionResponse), response.Headers);

            if (((int)response.StatusCode) >= 400)
                throw new ApiException((int)response.StatusCode, $"{transResp.ReturnCode} - {transResp.ReturnMessage}");
            else if (((int)response.StatusCode) == 0)
                throw new ApiException((int)response.StatusCode, $"{transResp.ReturnCode} - {transResp.ReturnMessage}");

            return transResp;
        }

        /// <summary>
        /// Cria uma Transação 
        /// </summary>
        /// <param name="transactionRequest">Transação</param>
        /// <returns>TransactionResponse</returns>
        public TransactionResponse CreateTransaction(TransactionRequest transaction)
        {
            var path = "/transactions";

            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            
            String postBody = ApiClient.Serialize(transaction); // http body (model) parameter

            headerParams.Add("authorization", ApiClient.ParameterToString(this.AccessToken)); // header parameter

            // authentication setting, if any
            String[] authSettings = new String[] { };

            // make the HTTP request
            IRestResponse response = (IRestResponse)ApiClient.CallApi(path, Method.POST, queryParams, postBody, headerParams, formParams, fileParams, authSettings);

            TransactionResponse transResp = (TransactionResponse)ApiClient.Deserialize(response.Content, typeof(TransactionResponse), response.Headers);

            if (((int)response.StatusCode) >= 400)
                throw new ApiException((int)response.StatusCode, $"{transResp.ReturnCode} - {transResp.ReturnMessage}");
            else if (((int)response.StatusCode) == 0)
                throw new ApiException((int)response.StatusCode, $"{transResp.ReturnCode} - {transResp.ReturnMessage}");

            return transResp;
        }

        /// <summary>
        /// Operação Obtém detalhes de uma Transação 
        /// </summary>
        /// <param name="tid">id da Transação</param> 
        /// <returns>TransactionResponse</returns>
        public TransactionResponseGet GetTransaction(string tid)
        {
            #region Validations
            // verify the required parameter 'id' is set
            if (string.IsNullOrEmpty(tid))
                throw new ApiException(400, "Missing required parameter 'tid' when calling GetTransaction");
            #endregion
            
            var path = $"/transactions/{tid}";
            
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
            
            headerParams.Add("authorization", ApiClient.ParameterToString(this.AccessToken)); // header parameter

            // authentication setting, if any
            String[] authSettings = new String[] { };

            // make the HTTP request
            IRestResponse response = (IRestResponse)ApiClient.CallApi(path, Method.GET, queryParams, postBody, headerParams, formParams, fileParams, authSettings);

            TransactionResponseGet transResp = (TransactionResponseGet)ApiClient.Deserialize(response.Content, typeof(TransactionResponseGet), response.Headers);

            if (((int)response.StatusCode) >= 400)
                throw new ApiException((int)response.StatusCode, $"{transResp.ReturnCode} - {transResp.ReturnMessage}");
            else if (((int)response.StatusCode) == 0)
                throw new ApiException((int)response.StatusCode, $"{transResp.ReturnCode} - {transResp.ReturnMessage}");

            return transResp;
        }

        /// <summary>
        /// Operação Obtém detalhes de uma Transação 
        /// </summary>
        /// <param name="redepayApiVersion">versão  da api</param> 
        /// <param name="id">id da Transação</param> 
        /// <param name="token">Token de acesso para quem esta efetuando a chamada aos recursos disponibilizados através da API. Tipo de parâmetro: header</param> 
        /// <returns>TransactionResponse</returns>
        public TransactionRefundResponse RefundTransaction(string tid, TransactionRefundRequest transRefundReq)
        {
            #region Validations
            // verify the required parameter 'idTransaction' is set
            if (string.IsNullOrEmpty(tid))
                throw new ApiException(400, "Missing required parameter 'tid' when calling RefundTransaction");

            // verify the required parameter 'Token' is set
            if (transRefundReq.Amount <= 0)
                throw new ApiException(400, "Missing required parameter 'amount' when calling RefundTransaction");
            #endregion
            
            var path = $"/transactions/{tid}/refunds";

            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            
            String postBody = ApiClient.Serialize(transRefundReq);

            //if (token != null) headerParams.Add("authorization", ApiClient.ParameterToString(token)); // header parameter
            headerParams.Add("authorization", ApiClient.ParameterToString(this.AccessToken)); // header parameter

            // authentication setting, if any
            String[] authSettings = new String[] { };

            // make the HTTP request
            IRestResponse response = (IRestResponse)ApiClient.CallApi(path, Method.POST, queryParams, postBody, headerParams, formParams, fileParams, authSettings);

            TransactionRefundResponse transResp = (TransactionRefundResponse)ApiClient.Deserialize(response.Content, typeof(TransactionRefundResponse), response.Headers);

            if (((int)response.StatusCode) >= 400)
                throw new ApiException((int)response.StatusCode, $"{transResp.ReturnCode} - {transResp.ReturnMessage}");
            else if (((int)response.StatusCode) == 0)
                throw new ApiException((int)response.StatusCode, $"{transResp.ReturnCode} - {transResp.ReturnMessage}");

            return transResp;
        }
		
    }
}

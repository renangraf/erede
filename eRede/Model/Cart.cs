﻿using System.Collections.Generic;

namespace eRede.Model
{
    public class Cart
    {
        public Consumidor Consumer { get; set; }
        public List<Endereco> Shipping { get; set; }
        public Endereco Billing { get; set; }
        public List<Item> Items { get; set; }
        public Ambiente Environment { get; set; }
    }
}

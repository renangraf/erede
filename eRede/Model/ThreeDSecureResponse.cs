﻿namespace eRede.Model
{
    public class ThreeDSecureResponse
    {
        public bool Embedded { get; set; }
        public string Url { get; set; }
    }
}

﻿namespace eRede.Model
{
    public class ThreeDSecure
    {
        public bool Embedded { get; set; }
        public string Eci { get; set; }
        public string Cavv { get; set; }
        public string Xid { get; set; }
        public string ReturnCode { get; set; }
        public string ReturnMessage { get; set; }
    }
}

﻿namespace eRede.Model
{
    public class Telefone
    {
        public TiposTelefones Type { get; set; }
        public string Ddd { get; set; }
        public string Number { get; set; }
    }
}

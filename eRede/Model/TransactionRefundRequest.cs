﻿using System.Collections.Generic;

namespace eRede.Model
{
    public class TransactionRefundRequest
    {
        public int Amount { get; set; }
        public List<Urls> Urls { get; set; }
    }
}

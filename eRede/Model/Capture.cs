﻿using System;

namespace eRede.Model
{
    public class Capture
    {
        public DateTime DateTime { get; set; }
        public string Nsu { get; set; }
        public int Amount { get; set; }
    }
}

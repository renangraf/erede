﻿namespace eRede.Model
{
    public class ConsumidorAmbiente
    {
        public string Ip { get; set; }
        public string SessionId { get; set; }
    }
}

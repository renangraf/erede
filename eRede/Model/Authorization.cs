﻿using System;

namespace eRede.Model
{
    public class Authorization
    {
        public DateTime DateTime { get; set; }
        public string ReturnCode { get; set; }
        public string ReturnMessage { get; set; }
        public int Affiliation { get; set; }
        // Status = Approved / Denied / Canceled / Pending
        public string Status { get; set; }
        public string Reference { get; set; }
        public string Tid { get; set; }
        public string Nsu { get; set; }
        public string AuthorizationCode { get; set; }
        public string Kind { get; set; }
        public int Amount { get; set; }
        public int Installments { get; set; }
        public string CardHolderName { get; set; }
        public string CardBin { get; set; }
        public string Last4 { get; set; }
        public string SoftDescriptor { get; set; }
        // Origin = e.Rede (1) / VISA Checkout (4) / MasterPass (6)
        public int Origin { get; set; }
        public bool Subscription { get; set; }
        public int DistributorAffiliation { get; set; }
    }
}

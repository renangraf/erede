﻿namespace eRede.Model
{
    public class ThreeDSecureRequest
    {
        public bool Embedded { get; set; }
        public string OnFailure { get; set; }
        public string UserAgent { get; set; }
    }
}

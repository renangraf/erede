﻿using System;

namespace eRede.Model
{
    public class Refund
    {
        public DateTime DateTime { get; set; }
        public string RefundId { get; set; }
        // Status = Done (Cancelamento efetivado) / Denied (Cancelamento negado) / Processing (Cancelamento em processamento)
        public string Status { get; set; }
        public int Amount { get; set; }
    }
}

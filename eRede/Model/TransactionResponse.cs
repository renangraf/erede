﻿using System;
using System.Collections.Generic;

namespace eRede.Model
{
    public class TransactionResponse
    {
        public string ReturnCode { get; set; }
        public string ReturnMessage { get; set; }
        public string Reference { get; set; }
        public string Tid { get; set; }
        public string Nsu { get; set; }
        public string AuthorizationCode { get; set; }
        public DateTime DateTime { get; set; }
        public int Amount { get; set; }
        public string CardBin { get; set; }
        public string Last4 { get; set; }

        public AntiFraudResponse Antifraud { get; set; }
        public ThreeDSecureResponse ThreeDSecure { get; set; }
        public List<Link> Links { get; set; }
    }
}

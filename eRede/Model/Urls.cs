﻿namespace eRede.Model
{
    public class Urls
    {
        public string Kind { get; set; }
        public string Url { get; set; }
    }
}

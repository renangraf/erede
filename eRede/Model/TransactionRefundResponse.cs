﻿using System;

namespace eRede.Model
{
    public class TransactionRefundResponse
    {
        public string ReturnCode { get; set; }
        public string ReturnMessage { get; set; }
        public string RefundId { get; set; }
        public string Tid { get; set; }
        public string Nsu { get; set; }
        public DateTime RefundDateTime { get; set; }
        public string CancelId { get; set; }
    }
}

﻿namespace eRede.Model
{
    public class Item
    {
        public TiposItens Type { get; set; }
        public string ShippingType { get; set; }
        public string Id { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public int Amount { get; set; }
        public int? Freight { get; set; }
        public int? Discount { get; set; }
    }
}

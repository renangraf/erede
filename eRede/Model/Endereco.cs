﻿namespace eRede.Model
{
    public class Endereco
    {
        public string AddresseeName { get; set; }
        public TiposEnderecos Type { get; set; }
        public string Address { get; set; }
        public string Number { get; set; }
        public string Complement { get; set; }
        public string ZipCode { get; set; }
        public string Neighbourhood { get; set; }
        public string City { get; set; }
        public string State { get; set; }
    }
}

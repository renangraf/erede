﻿namespace eRede.Model
{
    public class AntiFraudResponse
    {
        public decimal Score { get; set; }
        public string Recommendation { get; set; }
        public string Risklevel { get; set; }
        public bool Success { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace eRede.Model
{
    public class TransactionResponseGet
    {
        public DateTime RequestDateTime { get; set; }
        public string ReturnCode { get; set; }
        public string ReturnMessage { get; set; }

        public Authorization Authorization { get; set; }
        public Capture Capture { get; set; }
        public ThreeDSecure ThreeDSecure { get; set; }
        public AntiFraudResponse Antifraud { get; set; }
        public List<Refund> Refunds { get; set; }
        public List<Link> Links { get; set; }
    }
}
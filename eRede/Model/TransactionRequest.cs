﻿using System.Collections.Generic;

namespace eRede.Model
{
    public class TransactionRequest
    {
        public bool? Capture { get; set; }
        public string Kind { get; set; }
        public string Reference { get; set; }
        public int Amount { get; set; }
        public int? Installments { get; set; }
        public string CardHolderName { get; set; }
        public string CardNumber { get; set; }
        public int ExpirationMonth { get; set; }
        public int ExpirationYear { get; set; }
        public string SecurityCode { get; set; }
        public string SoftDescription { get; set; }
        public bool? Subscription { get; set; }
        public int? Origin { get; set; }
        public int? DistributorAffiliation { get; set; }
        public bool AntifraudRequired { get; set; }

        public ThreeDSecureRequest ThreeDSecure { get; set; }
        public Cart Cart { get; set; }
        public List<Urls> Urls { get; set; }
    }
}

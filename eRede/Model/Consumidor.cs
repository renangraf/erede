﻿//using System.Collections.Generic;

namespace eRede.Model
{
    public class Consumidor
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public char? Gender { get; set; }
        public string Cpf { get; set; }
        public Telefone Phone { get; set; }

        //public List<Documento> Documents { get; set; }
    }
}

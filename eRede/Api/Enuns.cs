﻿namespace eRede
{
    public enum EnvironmentType
    {
        Homologation = 0,
        Production = 1,
        Development = 2
    }
	
	public enum TiposEnderecos
    {
        Apartamento = 1,
        Casa = 2,
        Comercial = 3,
        Outro = 4
    }

    public enum TiposItens
    {
        Produtos = 1,
        ProdutosDigitais = 2,
        Servicos = 3,
        Aereas = 4
    }

    public enum TiposTelefones
    {
        Celular = 1,
        Residencial = 2,
        Comercial = 3,
        Outros = 4
    }
}

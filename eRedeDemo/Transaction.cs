﻿using eRede;
using eRede.Client;
using eRede.Model;
using System.Collections.Generic;

namespace eRedeDemo
{
    public class Transaction
    {
        const string Affiliation = "";
        const string Token = "";
        EnvironmentType EnvironmentType = EnvironmentType.Development;

        public TransactionResponseGet GetTransaction()
        {
            string IdTransaction = "8345000363484052380";

            TransactionResponseGet result = new TransactionResponseGet();

            Transactions trans = new Transactions(Affiliation, Token, EnvironmentType);

            result = trans.GetTransaction(IdTransaction);

            return result;
        }

        public TransactionResponse CreateTransaction()
        {
            TransactionResponse result = new TransactionResponse();

            TransactionRequest transReq = new TransactionRequest()
            {
                Capture = false,
                Reference = "teste123",
                Amount = 500900,
                Installments = 1,
                Kind = "credit", // credit / debit
                CardHolderName = "Joao das Neves",
                CardNumber = "5448280000000007",
                ExpirationMonth = 1,
                ExpirationYear = 2019,
                SecurityCode = "132",
                SoftDescription = "Soft Teste",
                DistributorAffiliation = int.Parse(Affiliation)
            };

            Transactions trans = new Transactions(Affiliation, Token, EnvironmentType);

            result = trans.CreateTransaction(transReq);

            return result;
        }

        public TransactionResponse CaptureTransaction()
        {
            string IdTransaction = "10117102821185500001";
            //string IdTransaction = "8345000363484052380";
            int Amount = 2099;

            TransactionResponse result = new TransactionResponse();

            Transactions trans = new Transactions(Affiliation, Token, EnvironmentType);

            result = trans.CaptureTransaction(IdTransaction, Amount);

            return result;
        }

        public TransactionRefundResponse RefundTransaction()
        {
            string IdTransaction = "10117102821185500001";
            //string IdTransaction = "8345000363484052380";
            
            TransactionRefundResponse result = new TransactionRefundResponse();

            TransactionRefundRequest transRefundReq = new TransactionRefundRequest()
            {
                Amount = 2099
            };

            // Example URL
            if (transRefundReq.Urls == null)
                transRefundReq.Urls = new List<Urls>()
                {
                    new Urls(){ Kind = "callback", Url = "www.url.com.br" }
                };
            //transRefundReq.Urls.Add(new Urls("www.url.com.br"));

            Transactions trans = new Transactions(Affiliation, Token, EnvironmentType);

            result = trans.RefundTransaction(IdTransaction, transRefundReq);

            return result;
        }

        public TransactionResponse CreateTransaction3DSecure()
        {
            TransactionResponse result = new TransactionResponse();

            TransactionRequest transReq = new TransactionRequest()
            {
                Capture = false,
                Reference = "tst1",
                Amount = 1100,
                //Installments = 1,
                Kind = "debit", // credit / debit
                CardHolderName = "Joao das Neves",
                CardNumber = "5277696455399733",
                ExpirationMonth = 1,
                ExpirationYear = 2019,
                SecurityCode = "123",
                SoftDescription = "Soft Teste",
                DistributorAffiliation = int.Parse(Affiliation),
                ThreeDSecure = new ThreeDSecureRequest()
                {
                    Embedded = true,
                    OnFailure = "decline",
                    UserAgent = "Mozilla/5.0 (iPad; U; CPU OS 3_2_1 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Mobile/7B405"
                },
                Urls = new List<Urls>()
                {
                    new Urls(){ Kind = "threeDSecureSuccess", Url = "https://redirecturl.com/3ds/success" },
                    new Urls(){ Kind = "threeDSecureFailure", Url = "https://redirecturl.com/3ds/failure" }
                }
            };

            Transactions trans = new Transactions(Affiliation, Token, EnvironmentType);

            result = trans.CreateTransaction(transReq);

            return result;
        }


        public TransactionResponse VerifyAntifraud()
        {
            TransactionRequest transAntifraud = new TransactionRequest()
            {
                Reference = "0000007",
                Amount = 11000,
                CardHolderName = "Pedro das Neves",
                Installments = 1,
                CardNumber = "5448280000000007",
                ExpirationMonth = 1,
                ExpirationYear = 2020,
                SecurityCode = "123",
                Capture = false,
                AntifraudRequired = true,
                Cart = new Cart()
                {
                    Consumer = new Consumidor()
                    {
                        Name = "Guilherme",
                        Email = "lorem@ipsum.com",
                        //Gender = 'M',
                        Cpf = "15153855406",
                        Phone = new Telefone()
                        {
                            Type = TiposTelefones.Celular,
                            Ddd = "011",
                            Number = "912341234"
                        }
                    },
                    //Shipping = new List<Endereco>()
                    //{
                    //    new Endereco()
                    //    {
                    //        AddresseeName = "Daenerys",
                    //        Type = TiposEnderecos.Casa,
                    //        Address = "Avenida Paulista",
                    //        Number = "123",
                    //        Complement = "Ap 2",
                    //        ZipCode = "01122123",
                    //        Neighbourhood = "Bela Vista",
                    //        City = "Sao Paulo",
                    //        State = "SP"
                    //    }
                    //},
                    //Billing = new Endereco()
                    //{
                    //    AddresseeName = "Daenerys",
                    //    Type = TiposEnderecos.Comercial,
                    //    Address = "Avenida Paulista",
                    //    Number = "123",
                    //    Complement = "Ap 2",
                    //    ZipCode = "01122123",
                    //    Neighbourhood = "Bela Vista",
                    //    City = "Sao Paulo",
                    //    State = "SP"
                    //},
                    //Items = new List<Item>()
                    //{
                    //    new Item()
                    //    {
                    //        Type = TiposItens.Servicos,
                    //        //Id = "123123",
                    //        Description = "instalar 3 tomadas",
                    //        Quantity = 1,
                    //        Amount = 6000,
                    //        //Freight = 199,
                    //        //Discount = 199
                    //    }
                    //},
                    //Environment = new Ambiente()
                    //{
                    //    Consumer = new ConsumidorAmbiente()
                    //    {
                    //        Ip = "192.168.137.212",
                    //        SessionId = "NomeEstabelecimento-WebSessionID"
                    //    }
                    //}
                }
            };

            Transactions trans = new Transactions(Affiliation, Token, EnvironmentType);

            TransactionResponse resp = trans.CreateTransaction(transAntifraud);

            return resp;
        }


        public TransactionResponse ZeroDollar()
        {
            TransactionRequest transZeroDollar = new TransactionRequest()
            {
                Reference = "1000000",
                Amount = 0,
                CardHolderName = "Pedro das Neves",
                Kind = "credit",
                CardNumber = "5448280000000007",
                ExpirationMonth = 1,
                ExpirationYear = 2020,
                SecurityCode = "123",
                Capture = true
            };

            Transactions trans = new Transactions(Affiliation, Token, EnvironmentType);

            TransactionResponse resp = trans.CreateTransaction(transZeroDollar);

            return resp;
        }

    }
}

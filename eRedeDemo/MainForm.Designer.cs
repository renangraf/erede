﻿namespace eRedeDemo
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CriarTransacao = new System.Windows.Forms.Button();
            this.CapturarTransacao = new System.Windows.Forms.Button();
            this.BuscarTransacao = new System.Windows.Forms.Button();
            this.EstornarTransacao = new System.Windows.Forms.Button();
            this.CriarTrans3DSecure = new System.Windows.Forms.Button();
            this.btnVerificarAntifraude = new System.Windows.Forms.Button();
            this.btnZeroDollar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // CriarTransacao
            // 
            this.CriarTransacao.Location = new System.Drawing.Point(128, 33);
            this.CriarTransacao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CriarTransacao.Name = "CriarTransacao";
            this.CriarTransacao.Size = new System.Drawing.Size(157, 28);
            this.CriarTransacao.TabIndex = 0;
            this.CriarTransacao.Text = "Criar Transação";
            this.CriarTransacao.UseVisualStyleBackColor = true;
            this.CriarTransacao.Click += new System.EventHandler(this.CriarTransacao_Click);
            // 
            // CapturarTransacao
            // 
            this.CapturarTransacao.Location = new System.Drawing.Point(128, 69);
            this.CapturarTransacao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CapturarTransacao.Name = "CapturarTransacao";
            this.CapturarTransacao.Size = new System.Drawing.Size(157, 28);
            this.CapturarTransacao.TabIndex = 1;
            this.CapturarTransacao.Text = "Capturar Transação";
            this.CapturarTransacao.UseVisualStyleBackColor = true;
            this.CapturarTransacao.Click += new System.EventHandler(this.CapturarTransacao_Click);
            // 
            // BuscarTransacao
            // 
            this.BuscarTransacao.Location = new System.Drawing.Point(128, 105);
            this.BuscarTransacao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BuscarTransacao.Name = "BuscarTransacao";
            this.BuscarTransacao.Size = new System.Drawing.Size(157, 28);
            this.BuscarTransacao.TabIndex = 2;
            this.BuscarTransacao.Text = "Buscar Transação";
            this.BuscarTransacao.UseVisualStyleBackColor = true;
            this.BuscarTransacao.Click += new System.EventHandler(this.BuscarTransacao_Click);
            // 
            // EstornarTransacao
            // 
            this.EstornarTransacao.Location = new System.Drawing.Point(128, 140);
            this.EstornarTransacao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.EstornarTransacao.Name = "EstornarTransacao";
            this.EstornarTransacao.Size = new System.Drawing.Size(157, 28);
            this.EstornarTransacao.TabIndex = 3;
            this.EstornarTransacao.Text = "Estornar Transação";
            this.EstornarTransacao.UseVisualStyleBackColor = true;
            this.EstornarTransacao.Click += new System.EventHandler(this.EstornarTransacao_Click);
            // 
            // CriarTrans3DSecure
            // 
            this.CriarTrans3DSecure.Location = new System.Drawing.Point(93, 176);
            this.CriarTrans3DSecure.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CriarTrans3DSecure.Name = "CriarTrans3DSecure";
            this.CriarTrans3DSecure.Size = new System.Drawing.Size(216, 28);
            this.CriarTrans3DSecure.TabIndex = 4;
            this.CriarTrans3DSecure.Text = "Criar Transação 3D Secure";
            this.CriarTrans3DSecure.UseVisualStyleBackColor = true;
            this.CriarTrans3DSecure.Click += new System.EventHandler(this.CriarTrans3DSecure_Click);
            // 
            // btnVerificarAntifraude
            // 
            this.btnVerificarAntifraude.Location = new System.Drawing.Point(128, 212);
            this.btnVerificarAntifraude.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnVerificarAntifraude.Name = "btnVerificarAntifraude";
            this.btnVerificarAntifraude.Size = new System.Drawing.Size(157, 28);
            this.btnVerificarAntifraude.TabIndex = 5;
            this.btnVerificarAntifraude.Text = "Verificar Antifraude";
            this.btnVerificarAntifraude.UseVisualStyleBackColor = true;
            this.btnVerificarAntifraude.Click += new System.EventHandler(this.BtnVerificarAntifraude_Click);
            // 
            // btnZeroDollar
            // 
            this.btnZeroDollar.Location = new System.Drawing.Point(128, 248);
            this.btnZeroDollar.Margin = new System.Windows.Forms.Padding(4);
            this.btnZeroDollar.Name = "btnZeroDollar";
            this.btnZeroDollar.Size = new System.Drawing.Size(157, 28);
            this.btnZeroDollar.TabIndex = 6;
            this.btnZeroDollar.Text = "Zero Dollar";
            this.btnZeroDollar.UseVisualStyleBackColor = true;
            this.btnZeroDollar.Click += new System.EventHandler(this.btnZeroDollar_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(396, 326);
            this.Controls.Add(this.btnZeroDollar);
            this.Controls.Add(this.btnVerificarAntifraude);
            this.Controls.Add(this.CriarTrans3DSecure);
            this.Controls.Add(this.EstornarTransacao);
            this.Controls.Add(this.BuscarTransacao);
            this.Controls.Add(this.CapturarTransacao);
            this.Controls.Add(this.CriarTransacao);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button CriarTransacao;
        private System.Windows.Forms.Button CapturarTransacao;
        private System.Windows.Forms.Button BuscarTransacao;
        private System.Windows.Forms.Button EstornarTransacao;
        private System.Windows.Forms.Button CriarTrans3DSecure;
        private System.Windows.Forms.Button btnVerificarAntifraude;
        private System.Windows.Forms.Button btnZeroDollar;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace eRedeDemo
{
    public partial class MainForm : Form
    {
        private Transaction transaction = null;

        public MainForm()
        {
            InitializeComponent();
            transaction = new Transaction();
        }
        
        private void CapturarTransacao_Click(object sender, EventArgs e)
        {
            transaction.CaptureTransaction();
        }

        private void CriarTransacao_Click(object sender, EventArgs e)
        {
            transaction.CreateTransaction();
        }

        private void BuscarTransacao_Click(object sender, EventArgs e)
        {
            transaction.GetTransaction();
        }

        private void EstornarTransacao_Click(object sender, EventArgs e)
        {
            transaction.RefundTransaction();
        }

        private void CriarTrans3DSecure_Click(object sender, EventArgs e)
        {
            transaction.CreateTransaction3DSecure();
        }

        private void BtnVerificarAntifraude_Click(object sender, EventArgs e)
        {
            transaction.VerifyAntifraud();
        }

        private void btnZeroDollar_Click(object sender, EventArgs e)
        {
            transaction.ZeroDollar();
        }
    }
}
